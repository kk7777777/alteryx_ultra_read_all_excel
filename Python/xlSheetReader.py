# author: Kacper Ksieski
# date: 2019-02-15
# description: output sheet names for any file of type xls, xlsx, xlsm, or xlsb
import sys
import xlrd
import openpyxl
from pyxlsb import open_workbook
import csv

def main(sourcePath, targetPath):
    if sourcePath[-5:] == '.xlsx' or sourcePath[-5:] == '.xlsm':
        xlTemp = openpyxl.load_workbook(sourcePath)
        sheetNames = xlTemp.sheetnames
    elif sourcePath[-5:] == '.xlsb':
        with open_workbook(sourcePath) as xlTemp:
            sheetNames = xlTemp.sheets
    elif sourcePath[-4:] == '.xls':
        xlTemp = xlrd.open_workbook(sourcePath)
        sheetNames = xlTemp.sheet_names()
    else:
        sheetNames = 'ERROR: file not recognized'

    sheetNames.insert(0, sourcePath)

    f = open(targetPath, 'w')
    with f:
        writer = csv.writer(f, delimiter='|', lineterminator='\n')
        writer.writerow(sheetNames)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
