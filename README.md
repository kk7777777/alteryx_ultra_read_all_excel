def sheets_to_dataframes(FullPath, sheets):
    sheetSeries = pd.Series(sheets)
    df_temp = pd.DataFrame({'FullPath': FullPath, 'SheetName': sheetSeries.values})
    return df_temp

def fetch_xls_sheets(FullPath):
    xlTemp = xlrd.open_workbook(FullPath)
    sheets = xlTemp.sheet_names()
    df_temp = sheets_to_dataframes(FullPath, sheets)
    return df_temp

def fetch_xlsb_sheets(FullPath):
    with open_workbook(FullPath) as xlTemp:
        sheets = xlTemp.sheets
        df_temp = sheets_to_dataframes(FullPath, sheets)
        return df_temp

def fetch_xlsx_xlsm_sheets(FullPath):
    warnings.filterwarnings("ignore",category=UserWarning)
    xlTemp = openpyxl.load_workbook(FullPath)
    sheets = xlTemp.sheetnames
    df_temp = sheets_to_dataframes(FullPath, sheets)
    return df_temp
    

df_out = []

file_array = df_in['FullPath']
files = pd.Series(file_array)
function_call_array = df_in['function_call']
function_calls = pd.Series(function_call_array)

for index, file in enumerate(files):
    function_call = function_calls[index]
    df_sheets = eval(function_call)(file)
    df_out.append(df_sheets)

df_out = pd.concat(df_out, ignore_index=True, sort=False)



df_out = []
df_sheets = pd.DataFrame()

for row in df_in.itertuples(index=True):
    if row.function_call == 'fetch_xlsx_xlsm_sheets':
        df_sheets = fetch_xlsx_xlsm_sheets(row.FullPath)
    elif row.function_call == 'fetch_xls_sheets':
        df_sheets = fetch_xls_sheets(row.FullPath)
    elif row.function_call == 'fetch_xls_sheets':
        df_sheets = fetch_xls_sheets(row.FullPath)
    else:
        None
    df_out.append(df_sheets)

df_out = pd.concat(df_out, ignore_index=True, sort=False)


OLDER VERSION
#################################
from ayx import Package


#################################
from ayx import Alteryx
import xlrd
import openpyxl
from pyxlsb import open_workbook
import pandas as pd
import warnings


#################################
def fetch_xls_sheets(FullPath):
    xlTemp = xlrd.open_workbook(FullPath)
    sheets = xlTemp.sheet_names()
    sheetSeries = pd.Series(sheets)
    df_temp = pd.DataFrame({'FullPath': FullPath, 'SheetName': sheetSeries.values})
    return df_temp

def fetch_xlsb_sheets(FullPath):
    with open_workbook(FullPath) as xlTemp:
        sheets = xlTemp.sheets
        sheetSeries = pd.Series(sheets)
        df_temp = pd.DataFrame({'FullPath': FullPath, 'SheetName': sheetSeries.values})
        return df_temp

def fetch_xlsx_xlsm_sheets(FullPath):
    warnings.filterwarnings("ignore",category=UserWarning)
    xlTemp = openpyxl.load_workbook(FullPath)
    sheets = xlTemp.sheetnames
    sheetSeries = pd.Series(sheets)
    df_temp = pd.DataFrame({'FullPath': FullPath, 'SheetName': sheetSeries.values})
    return df_temp


#################################
df_in = Alteryx.read("#1")


#################################
df_out = []

file_array = df_in['FullPath']
files = pd.Series(file_array)
function_call_array = df_in['function_call']
function_calls = pd.Series(function_call_array)

for index, file in enumerate(files):
    function_call = function_calls[index]
    df_sheets = eval(function_call)(file)
    df_out.append(df_sheets)
    
df_out = pd.concat(df_out, ignore_index=True, sort=False)


#################################
md = Alteryx.readMetadata('#1')
del md['function_call']
md['SheetName'] = {
    'name': 'SheetName', 
    'type': 'V_WString', 
    'length': 1073741823,
    'description': 'script author: Kacper Ksieski'
}
md


#################################
Alteryx.write(df_out, 1, md)



## Service Desk
To open an issue, please send an email to:

incoming+kk7777777-alteryx-ultra-read-all-excel-11360840-issue-@incoming.gitlab.com